package com.example.tests;


import com.example.RoboDemo.MainActivity;
import com.example.RobolectricGradleTestRunner;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.util.ActivityController;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@RunWith(RobolectricGradleTestRunner.class)
public class Sample {
    @Test
    public void justRunDamnit() throws Exception{
        assertTrue("Expected a hard truth", true);
    }

    @Test
    public void activitySlappity() throws Exception {
        MainActivity a = Robolectric.buildActivity(MainActivity.class).create().get();
        assertNotNull("Should have had an activity.", a);
    }

}
